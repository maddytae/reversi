#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#some random line
"""
Created on Sat Dec 16 15:55:13 2017

@author: maddy
"""

from matplotlib import pyplot as plt
import matplotlib.animation as animation
import pandas as pd
import random
import time
import strategy as st
import elements as el
import os

#random.seed(47)


#plotting figure
fig, ax = plt.subplots()

ax.axis('off')
#fig.add_subplot(122)

 
#resetting of graph

ax.set_xlim([-40, 80])
ax.set_ylim([-40, 50])
ax.set_aspect('equal')    
for i in range(8):
        ax.annotate(el.tag2[i+1], ((-29+i*8), 33), textcoords='data', size=10)
        ax.annotate(i+1, (-35, (27-i*8)), textcoords='data', size=10)
for i in range(-4,5):
    plt.plot((-32,32),(i*8,i*8),linewidth=1.5,color='k')
    plt.plot((i*8,i*8),(-32,32),linewidth=1.5,color='k')    

def reset_graph():
    reset = plt.Rectangle((-32, -32), 64, 64, fc='green')
    ax.add_patch(reset)
    
    
reset_graph()
#change this based on game data. ie check if simulation was run with black as first player or white as first player

player='white'

#initial values
el.cells_values['D4']=-1
el.cells_values['E4']=1
el.cells_values['D5']=1
el.cells_values['E5']=-1

def placement(i,j,p):
    global player
    #dictionaries are global by nature and hence is not req to be declared as global
         
    if p=='black': 
        el.cells_values[el.RC_cell[str(i)+str(j)]]=-1
        token=-1
    else:  
        el.cells_values[el.RC_cell[str(i)+str(j)]]=1
        token=1
    #later check whether any valid move is left to be played
    if p=='white':
        
        player='black'
    else:
       
        player='white'
        
    for x in range(-1,2):
        for y in range(-1,2):
            if not(x ==0 and y ==0):
                u=1
                while i+x*u>=1 and i+x*u<=8 and j+y*u>=1 and j+y*u<=8 and el.RC_get_v(i+x*u,j+y*u)!=0 and el.RC_get_v(i+x*u,j+y*u)!=token:
                    u=u+1
                if u!=1 and i+x*u>=1 and i+x*u<=8 and j+y*u>=1 and j+y*u<=8 and el.RC_get_v(i+x*u,j+y*u)!=0:
                    #print(u)
                    for v in range(1,u+1):
                        el.cells_values[el.RC_cell[str(i+x*v)+str(j+y*v)]]=token
                       
                        q,r=el.cells_position[el.RC_cell[str(i+x*v)+str(j+y*v)]]
                        circle=plt.Circle((q,r),2.5,color=p)
                        ax.add_patch(circle)

#when called scans whole board and highlights valid possible moves that a player can make                       

rect_list=[]

    
z=0
for i in range(1,9):
    for j in range(1,9):
        x='rect'+str(i)+str(j)
        rect_list.append(x)
        rect_list[z]=plt.Rectangle((0,0), 8, 8, fc='blue')
        ax.add_patch(rect_list[z])
        z=z+1

#this only returns list of possible moves
def return_possible_moves_temp(p):    
    poss_moves=[]
    z=0
    for i in range(1,9):
        for j in range(1,9):
            if el.valid_possible_moves(i,j,p)==True:
                x,y=el.RC_position[str(i)+str(j)]
                poss_moves.append(el.RC_cell[str(i)+str(j)])
            z=z+1    
    return poss_moves
#this returns and also highlights list of possible moves
def highlight_possible_moves_temp(p):
    
    poss_moves=[]
    z=0
    for i in range(1,9):
        for j in range(1,9):
            rect_list[z].set_color('green')          
            if el.valid_possible_moves(i,j,p)==True:
                x,y=el.RC_position[str(i)+str(j)]
                rect_list[z].set_color('blue')
                rect_list[z].set_x(x-4)
                rect_list[z].set_y(y-4)                
                #rectangle = plt.Rectangle((x-4,y-4), 8, 8, fc='blue')
                #ax.add_patch(rectangle)                
                poss_moves.append(el.RC_cell[str(i)+str(j)])
            z=z+1
    
    return poss_moves               
#highlight_possible_moves(player)    
end_flag=0
#this is required if the player has no valid move to make, the turn will switch
def highlight_possible_moves(p):
    global end_flag
    global player
    if p=='white':
        anti_p='black'
    else:
        anti_p='white'
    
    k=highlight_possible_moves_temp(p)
    if len(k)==0: 
        if len(return_possible_moves_temp(anti_p))==0:
            end_flag=1                   
        if p=='white':
            player='black'                       
        if p=='black':
            player='white'
        k=highlight_possible_moves_temp(player)    
    return k
 
                
#draws a board based on black, white and possible valid moves
  
def board(p):
    #reset_graph()
    for k,v in el.cells_values.items():
        
        if v==1:
            x,y=el.cells_position[k]
            
            circle=plt.Circle((x,y),2.5,color='white')
            ax.add_patch(circle)
        if v==-1:
            x,y=el.cells_position[k]
            
            circle=plt.Circle((x,y),2.5,color='black')
            ax.add_patch(circle)
        
 
#all about annotations

player1=ax.annotate('', (35,30), textcoords='data', size=10)
player2=ax.annotate('', (35,25), textcoords='data', size=10)
turn=ax.annotate('', (35,20), textcoords='data', size=10)
move_count=ax.annotate('', (35,15), textcoords='data', size=10)
ax.annotate('Valid moves:', (35,10), textcoords='data', size=10)
winner=ax.annotate('', (35,-25), textcoords='data', size=15,color='red')



yy=[]
tag=5
for i in range(5):
    
    yy.append(ax.annotate('', (35,tag), textcoords='data',size=7   ))
    tag=tag-5

    
def annotate(p,k=1):
    global c_move
    poss_list=highlight_possible_moves(p)
    
    poss_sub_list = [poss_list[x:x+5] for x in range(0, len(poss_list), 5)]     
    player1.set_text('White score:'+str(sum(x==1 for x in el.cells_values.values())))
    player2.set_text('Black score:'+str(sum(x==-1 for x in el.cells_values.values())))    
    move_count.set_text(k)  
    #c_move=c_move+1-1  
    if len(poss_sub_list)>0:
        turn.set_text('Next move by: '+str.title(player))
    
    for i in range(5):
        yy[i].set_text('')
    
    for i in range(len(poss_sub_list)):
        yy[i].set_text(poss_sub_list[i])
       
        #yy[i].set_text(max_score(poss_sub_list[i])[1])
        #yy[i].set_text(score)
    return poss_list
    
#initial state 
   
board(player) 
highlight_possible_moves(player)        
p_move=annotate(player)
  


yf=[]
q=1   
e=0
#this initiation is helpful for single plays. 
combined_dict={**st.x_dict,**st.y_dict,**st.z_dict,**st.s_dict,**st.a_dict,**st.b_dict,**st.c_dict,**st.rest_dict}


ef=pd.read_csv(os.path.join(os.getcwd(),'out.csv'))
#simulation number k from ef game database
def on_game_data(event,k=853,df=ef):
    #global e
    global q
    
    p_move=annotate(player,q)
    #s=random.choice(p_move)
   
    #centre=cells_position[s] 
    #centre=position(event.xdata,event.ydata)
    df_list=df.loc[ef['Simulation'] ==k].values.tolist()
    #remember once you select cell manually, the control will be shifted away from game data
    if event.xdata>-32 and event.xdata<32 and event.ydata>-32 and event.ydata<32:
        centre=el.position(event.xdata,event.ydata)
        
    else:       
    
        centre=el.cells_position[df_list[q-1][3]]
    
    if el.return_key(centre) in p_move:
        #yf.append([q,player,return_key(centre),len(p_move),sum(x==1 for x in cells_values.values()),sum(x==-1 for x in cells_values.values())])    
        #print(yf[q])
        #print(max_score(p_move))
        q=q+1                 
        x,y=el.cell_RC[el.return_key(centre)]
        r,c=int(x),int(y)
        ###this circle can be made more efficient
        circle=plt.Circle((centre[0],centre[1]),2.5,color=player)
        ax.add_patch(circle)        
        placement(r,c,player)                
        p_move=annotate(player,q)           
        #fig.canvas.draw()
#cid = fig.canvas.mpl_connect('button_press_event',on_game_data)

def against_ai(event):
    
    global q
    
    p_move=annotate(player,q)
    if player=='white':
        centre=el.position(event.xdata,event.ydata)
        #scoring={'random':'random'}
        #print(scoring)
    else:
        #centre=el.position(event.xdata,event.ydata)
        centre=el.cells_position[st.max_score(el.cells_values,combined_dict,p_move)[0]]
        #scoring=max_score(p_move)[1]
        #print(scoring)
   
    if el.return_key(centre) in p_move:
        yf.append([q,player,el.return_key(centre),len(p_move),sum(x==1 for x in el.cells_values.values()),sum(x==-1 for x in el.cells_values.values())])    
        q=q+1                 
        x,y=el.cell_RC[el.return_key(centre)]
        r,c=int(x),int(y)
        ###this circle can be made more efficient
        circle=plt.Circle((centre[0],centre[1]),2.5,color=player)
        ax.add_patch(circle) 
        placement(r,c,player)               
        p_move=annotate(player,q)           
        fig.canvas.draw()
cid = fig.canvas.mpl_connect('button_press_event',against_ai)

def simulate_play(*args):
    
    global e
    global q
    
   
    scoring={}
    
    p_move=annotate(player)
    #print(p_move)
    if player=='black':
        #s=st.max_score(el.cells_values,combined_dict,p_move)[0]
        s=random.choice(p_move)
        #scoring=st.max_score(el.cells_values,combined_dict,p_move)[1]
        scoring={'random':'random'}
    else:
        s=st.max_score(el.cells_values,combined_dict,p_move)[0]
        #s=random.choice(p_move)
        scoring=st.max_score(el.cells_values,combined_dict,p_move)[1]
        #scoring={'random':'random'}
    
    
    centre=el.cells_position[s] 
    #centre=position(event.xdata,event.ydata)
                
    x,y=el.cell_RC[el.return_key(centre)]
    r,c=int(x),int(y)
    ###this circle can be made more efficient
    circle=plt.Circle((centre[0],centre[1]),2.5,color=player)
    ax.add_patch(circle)        
    u=player  
    placement(r,c,player)               
    p_move=annotate(player,q)   
    yf.append([q,u,s,len(p_move),sum(x==1 for x in el.cells_values.values()),sum(x==-1 for x in el.cells_values.values()),scoring])    
    q=q+1        
    #fig.canvas.draw()
#ani = animation.FuncAnimation(fig, simulate_play, interval=5, blit=False)


#simulation starts here   

def run_simulation(p,s,status):
    global end_flag,q,yf,p_move,player,combined_dict
    #initial state
    board(player) 
    highlight_possible_moves(player)        
    p_move=annotate(player)
    
    game_data=[]
    start=time.time()
    in_start=start
    w=0
    b=0
    d=0
    combined_dict={**st.x_dict,**st.y_dict,**st.z_dict,**st.s_dict,**st.a_dict,**st.b_dict,**st.c_dict,**st.rest_dict}
    for i in range(s):
       
    
    
        if i%(status)==0:
            print(str(round((i/s)*100,2))+'% completed in '+str(round(time.time()-start,2))+' seconds.......')
            print('White:'+str(w)+' Black:'+str(b)+' Draw:'+str(d))
            start=time.time()
        if end_flag==0:
        
            while end_flag!=1:
                simulate_play()
  
        end_flag=0
    
        q=1
        for item in yf:
            item.insert(0,i+1)
            game_data.append(item)
        if yf[-1][5]>yf[-1][6]:
            w=w+1
        elif yf[-1][5]<yf[-1][6]:
            b=b+1
        else:
            d=d+1
        
        yf=[]
        combined_dict={**st.x_dict,**st.y_dict,**st.z_dict,**st.s_dict,**st.a_dict,**st.b_dict,**st.c_dict,**st.rest_dict}
        
        #premature break to avoid unnecessary steps during last simulation
        if i==s-1:
            break
    
        #print(plt.bar(y_pos, performance, align='center', alpha=0.5)'i in middle '+str(i))   
        for k in el.cells_values:
            el.cells_values[k]=0
        
        player=p
        el.cells_values['D4']=-1
        el.cells_values['E4']=1
        el.cells_values['D5']=1
        el.cells_values['E5']=-1
        board(player) 
        highlight_possible_moves(player)        
        p_move=annotate(player)
        #print('i at end '+str(i))
    print('Final Score: White: '+str(w)+' Black: '+str(b)+' Draw: '+str(d))
    print('Total time taken to complete simulation is '+str(round(time.time()-in_start,2))+' seconds.')
    df = pd.DataFrame(game_data, columns=['Simulation','move','player','cell','#ofp_moves','white score','black score','Scoring'])
    return df.to_csv('out.csv',index=False)
#run_simulation('white',100,10)
against_ai
