#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  3 09:44:01 2018

@author: maddy
"""

#strategy section
x_set=['B2','G2','B7','G7']
y_set=['A2','A7','B1','B8','G1','G8','H2','H7']
z_set=['A1','A8','H1','H8']
s_set=['C3','F3','C6','F6']
a_set=['C1','F1','A3','H3','A6','H6','C8','F8']
b_set=['D1','E1','A4','A5','H4','H5','D8','E8']
c_set=['B3','C2','B6','C7','F2','G3','F7','G6']
rest_set=['D2','D3','D4','D5','D6','D7','E2','E3','E4','E5','E6','E7','B4','C4','F4','G4','B5','C5','F5','G5']

x_set_v=-98
y_set_v=-99
z_set_v=99
s_set_v=50
a_set_v=40
b_set_v=20
c_set_v=70
#rest_set_v=30
rest_set_v=60

x_dict=dict.fromkeys(x_set, x_set_v)
y_dict=dict.fromkeys(y_set, y_set_v)
z_dict=dict.fromkeys(z_set, z_set_v)
s_dict=dict.fromkeys(s_set, s_set_v)
a_dict=dict.fromkeys(a_set, a_set_v)
b_dict=dict.fromkeys(b_set, b_set_v)
c_dict=dict.fromkeys(c_set, c_set_v)
rest_dict=dict.fromkeys(rest_set, rest_set_v)



"""for items in x_set:
    ax.annotate('x', cells_position[items], textcoords='data', size=10)
for items in y_set:
    ax.annotate('y', cells_position[items], textcoords='data', size=10)
for items in z_set:
    ax.annotate('z', cells_position[items], textcoords='data', size=10)
for items in s_set:
    ax.annotate('s', cells_position[items], textcoords='data', size=10)
for items in a_set:
    ax.annotate('a', cells_position[items], textcoords='data', size=10)
for items in b_set:
    ax.annotate('b', cells_position[items], textcoords='data', size=10)
for items in c_set:
    ax.annotate('c', cells_position[items], textcoords='data', size=10)
    """
       
def max_score(c_v,c_d,move):   
    
    wedge_value=98
    if c_v['A1']==c_v['C1'] and c_v['A1']!=0:
        c_d['B1']=wedge_value
    if c_v['A1']==c_v['A3'] and c_v['A1']!=0:
       c_d['A2']=wedge_value  
    if c_v['H1']==c_v['F1'] and c_v['H1']!=0:
        c_d['G1']=wedge_value
    if c_v['H1']==c_v['H3'] and c_v['H1']!=0:
        c_d['H2']=wedge_value     
    if c_v['A8']==c_v['A6'] and c_v['A8']!=0:
        c_d['A7']=wedge_value
    if c_v['A8']==c_v['C8'] and c_v['A8']!=0:
        c_d['B8']=wedge_value
    if c_v['H8']==c_v['H6'] and c_v['H8']!=0:
        c_d['H7']=wedge_value  
    if c_v['H8']==c_v['F8'] and c_v['H8']!=0:
        c_d['G8']=wedge_value 
    x={k:c_d[k] for k in move}
    
    #print(max(x,key=x.get))
    v=max(x,key=x.get)
    
    return v,x
"""    
Future dev goals:
1) Make circle patches more efficient so that board can be plotted based on cell values
2) Robust step 1 is required so that effective look up can be implemented  
3) look up: a) look if opponent has any strategic advantage, b) for first 20 or so  give weightage to number of pieces, less the better  
    
"""