#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  3 10:19:56 2018

@author: maddy
"""
#necessary variable declarations to fecilitate developement 

tag1={'A':1,'B':2,'C':3,'D':4,'E':5,'F':6,'G':7,'H':8}
tag2={1:'A',2:'B',3:'C',4:'D',5:'E',6:'F',7:'G',8:'H'}

#list of coordinates for centres of each cell
center_list=[]
for i in range(-28,29,8):
    for j in range(-28,29,8):        
        center_list.append((i,j))
        

tag=[]
point=[]
value=[]
RC=[]
#for Row and Column style refrencing
for i in range(8):
    for j in range(8):        
        RC.append(str((j+1)*10+i+1))
        
for i in range(-4,4):
    for j in range(-4,4):
        x,y=8*i+4,8*(-j)-4
        co_ord=[x,y]
        tag.append(tag2[i+5]+str(j+5))
        point.append(co_ord)
        value.append(0)
        
#dicts to fecilitate easy referencings      
cells_position = dict(zip(tag, point)) 
cells_values=dict(zip(tag, value))
RC_cell=dict(zip(RC, tag))
cell_RC=dict(zip(tag, RC))
RC_position=dict(zip(RC, point))

x=[item[0] for item in point]
y=[item[1] for item in point]
cell_x=dict(zip(tag, [item[0] for item in point]))
cell_y=dict(zip(tag, [item[1] for item in point]))


def position(x,y):
    d=[]
    pos=0
    for k in range(64):
        d.append(((x-center_list[k][0])**2+(y-center_list[k][1])**2)**0.5)
    pos=d.index(min(d))
    return [center_list[pos][0],center_list[pos][1]]

def return_key(x):
    
    for k,v in cells_position.items():
        key='None'
        if v==x:
            key=k
            break
    return key

def RC_get_v(x,y):
    return cells_values[RC_cell[str(x)+str(y)]]



def valid_possible_moves(i,j,player):
    
    if RC_get_v(i,j)!=0:  
        return False
  
    if player=='black': 
        token=-1
    else:  
        token=1
  
    value=False
    for x in range(-1,2):
        for y in range(-1,2):
            if not(x ==0 and y ==0):
                u=1
                while i+x*u>=1 and i+x*u<=8 and j+y*u>=1 and j+y*u<=8 and RC_get_v(i+x*u,j+y*u)!=0 and RC_get_v(i+x*u,j+y*u)!=token:
                    u=u+1
                if u!=1 and i+x*u>=1 and i+x*u<=8 and j+y*u>=1 and j+y*u<=8 and RC_get_v(i+x*u,j+y*u)!=0:
                    value=True
    return value